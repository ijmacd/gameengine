GameEngine
==========

A highly configurable, extremely modular Javascript game engine

Sample
------

The current state of the latest features in the game engine are tested in the `test` folder.
Check there for samples, examples and other fun.
Live demo of the test folder at http://ijmacd.github.io/GameEngine/test

* [Solar System](http://ijmacd.github.io/GameEngine/test/solarSystem.html) - Demonstration Solar System
