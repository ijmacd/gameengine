var GE = (function(GE){
  "use strict";

  GE.Comp = GE.Comp || {};

  var GameObject = GE.GameObject,
      GameComponent = GE.GameComponent,
      GEC = GE.Comp;

  /***********************
   * Module code goes here
   ***********************/

  return GE;
}(GE || {}));
